---
version: "3.3"
services:
  backend:
    image: ${BACKEND_IMAGE_NAME}
    container_name: backend-server
    ports:
      - 10350:10350
    environment:
      BASE_URL: http://localhost:10350/
      MONGO_URI: mongodb://backend-database
      RABBITMQ_URL: amqp://rabbitmq
      TIME_OUT: 20000
    depends_on:
      - backend-database
      - rabbitmq
    networks:
      - backend-network
      - testing-network

  backend-database:
    image: mongo:4
    container_name: backend-database_test-runner
    healthcheck:
      test: ["CMD", "mongo", "--eval", "db.runCommand({ping:1})"]
      interval: 20s
      timeout: 20s
      retries: 3
      start_period: 40s
    networks:
      - backend-network

  # https://docs.docker.com/compose/compose-file/
  rabbitmq:
    image: 'rabbitmq:3.6-management-alpine'
    container_name: rabbitmq_test-runner
    ports:
      # The standard AMQP protocol port
      - '5672:5672'
      # HTTP management UI
      - '15672:15672'
    environment:
      # The location of the RabbitMQ server.  "amqp" is the protocol;
      # "rabbitmq" is the hostname.  Note that there is not a guarantee
      # that the server will start first!  Telling the pika client library
      # to try multiple times gets around this ordering issue.
      AMQP_URL: 'amqp://rabbitmq?connection_attempts=5&retry_delay=5'
      RABBITMQ_DEFAULT_USER: "guest"
      RABBITMQ_DEFAULT_PASS: "guest"
    networks:
      - backend-network

  test-runner:
    image: test-runner:latest
    container_name: test-runner
    build: ./
    environment:
      SUT_BASE_URL: http://backend-server:10350
      SUT_MONGO_URL: mongodb://backend-database
      SUT_RABBITMQ_URL: amqp://rabbitmq
      SUT_MONGO_DATABASE: items
      SUT_MONGO_COLLECTION: items
      TEST_TIMEOUT_MS: 5000
    volumes:
      - type: bind
        source: ./
        target: /app/tests
        read_only: true
      - type: bind
        source: ./openapi.yaml
        target: /openapi.yaml
        read_only: true
    depends_on:
      backend:
        condition: service_started
      backend-database:
        condition: service_healthy
    networks:
      - testing-network

networks:
  backend-network:
    name: backend-network
  testing-network:
    name: testing-network
