const { getAPIVersion } = require('./lib/api.js')
const yaml = require('js-yaml')
const fs = require('fs')

const openapi = yaml.safeLoad(fs.readFileSync('./lib/openapi.yaml', 'utf8'))
const expectedVersion = openapi.info.version

describe('test getAPIVersion GET /version', function () {
    it('should return the correct API version', async function () {
        const res = await getAPIVersion()
        res.should.be.a.status(200)
        // expect version to be in range ['0.0.0', '99.99.99']
        res.should.have.a.property('text').with.length.above(4).and.below(9)
        res.text.should.equal(expectedVersion)
    })
})
