const { listGuests, createGuest, deleteGuest } = require('./lib/api.js')

describe('test listGuest GET /guests', function () {
    // guest list
    const guestDataOne = {
        wsuID: '1234560',
        resident: true,
        zipCode: '01602',
        unemployment: false,
        assistance: {
            socSec: false,
            TANF: false,
            finAid: false,
            other: false,
            SNAP: false,
            WIC: false,
            breakfast: false,
            lunch: false,
            SFSP: false,
        },
        guestAge: 42,
        numberInHousehold: 4,
    }

    const guestDataTwo = {
        wsuID: '6666666',
        resident: false,
        zipCode: '90210',
        unemployment: true,
        assistance: {
            socSec: true,
            TANF: false,
            finAid: true,
            other: false,
            SNAP: true,
            WIC: false,
            breakfast: true,
            lunch: true,
            SFSP: false,
        },
        guestAge: 30,
        numberInHousehold: 2,
    }

    before(async function () {
        // Create a guest before running the tests
        await createGuest(guestDataOne)
        await createGuest(guestDataTwo)
    })

    after(async function () {
        // Delete guest after running the tests
        await deleteGuest(guestDataOne.wsuID)
        await deleteGuest(guestDataTwo.wsuID)
    })

    it('should return complete list of guest', async function () {
        const res = await listGuests(200)
        res.should.have.status(200)
        res.body.should.be.an('array').with.lengthOf(2)
        res.body[0].should.deep.include(guestDataOne)
        res.body[1].should.deep.include(guestDataTwo)
    })

    it('should return an error if the guest path is invalid', async function () {
        const res = await listGuests(400)
        res.should.have.status(400)
        res.body.should.have
            .property('message')
            .equal('request.params.wsuID should match pattern "^[0-9]{7}$"')
        res.body.errors[0].should.have.own
            .property('errorCode')
            .equal('pattern.openapi.validation')
    })
})
