const { createGuest, getGuest, deleteGuest } = require('./lib/api.js');
const should = require('chai').should();
let createdGuestId;

describe('test retrieveGuest GET /guests/{wsuID}', function () {
    const guestData = {
        wsuID: '1234563',
        resident: true,
        zipCode: '01602',
        unemployment: false,
        assistance: {
            socSec: false,
            TANF: false,
            finAid: false,
            other: false,
            SNAP: false,
            WIC: false,
            breakfast: false,
            lunch: false,
            SFSP: false,
        },
        guestAge: 42,
        numberInHousehold: 4,
    }

    before(async function () {
        // Create a guest before running the tests
        await createGuest(guestData)
        createdGuestId = guestData.wsuID
    })

    after(async function () {
        // Delete guest after running the tests
        await deleteGuest(createdGuestId)
    })

    it('should return the guest with the given ID', async function () {
        // Get the guest that was created in the before function
        const res = await getGuest(createdGuestId)
        res.should.have.status(200)
        delete res.body.firstVisitDate;
        res.body.should.deep.equal(guestData)
    })

    it('should report that guest is not found', async function () {
        // Get the guest that was created in the before function
        let notFoundGuestId = '7234567'
        const res = await getGuest(notFoundGuestId)
        res.should.have.status(404)
        res.body.should.have.property('error').equal('Guest not found')
        res.body.should.have.property('message').equal('wsuID does not exist')
    })

    it('should return an error if the guest ID is invalid', async function () {
        const invalidGuestId = 'abc123'
        const res = await getGuest(invalidGuestId)
        res.should.have.status(400)
        res.body.should.have
            .property('message')
            .equal('request.params.wsuID should match pattern "^[0-9]{7}$"')
        res.body.errors[0].should.have.own
            .property('errorCode')
            .equal('pattern.openapi.validation')
    })
})
