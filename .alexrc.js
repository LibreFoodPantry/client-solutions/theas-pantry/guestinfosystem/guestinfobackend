exports.allow = [
    //identify a set of rules we want Alex linter to ignore
]

exports.noBinary = false; //boolean set to false by default which determines whether linter flags phrases like "he or she"

exports.profanitySureness = 1; //Set with a number between {0, 2}
